 #include <stdio.h>
#include <stdlib.h>

//Utilizando la recursividad para ir haciendo la suma haciendo la comparacion del numero
int fibonacci(int n)
{
  if (n>2)
    return fibonacci(n-1) + fibonacci(n-2);
  else if (n==2)
    return 1;
  else if (n==1)       
    return 1;
 else if (n==0)
    return 0;
}

int main(void)
{
    int num;
    int i;
printf("N Numeros\n");
scanf("%d",&num);

//Muestra los numeros Fibonacci desde 1
    for (i=1; i<=num; i++)
    {
      printf("%d\n", fibonacci(i));
    }
 
    
  return 0;
}
